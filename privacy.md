# CICO Browser Privacy Policy

The following privacy policy is updated on April 12, 2020.

Welcome to our App. CICO Browser is a clean web browser with easy content blocking. You can easily block AD, image or video as you like. We don't collect any personal information, it's safe for you to use our App.