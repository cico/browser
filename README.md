# CICO Browser

![](https://gitlab.com/cico/browser/raw/master/58x58.png)

CICO Browser is a clean web browser with easy content blocking. You can easily block AD, image or video as you like.

## App Store
- Version: 1.2.16
- [Click here to install](https://apps.apple.com/app/id1507305150)

## Ad Hoc
- Version: 2.0.18
- [Click here to install](itms-services:///?action=download-manifest&url=https://gitlab.com/cico/browser/raw/master/adhoc/manifest.plist)

## Enterprise
- Version: 1.1.15
- [Click here to install](itms-services:///?action=download-manifest&url=https://gitlab.com/cico/browser/raw/master/enterprise/manifest.plist)
